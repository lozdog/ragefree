package com.adams.rageFree;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Lawrence Adams
 * @version 0.1.2WORKING
 * @revision 2
 * @since 18/02/2013
 * @working == true
 */

/*
 * Built for JRE Version 1.6 Using JDK 1.7.0_10 with compiler compliance for JRE
 * 1.6
 */

public class Menu {

	static Thread discoveryThread = new Thread(ServerThread.getInstance());

	public static void main(String[] args) {
		
		System.out
				.println("                            ___________                      ");
		System.out
				.println("____________     ____   ____\\_   _____/______   ____   ____  ");
		System.out
				.println("\\_  __ \\__  \\   / ___\\_/ __ \\|    __) \\_  __ \\_/ __ \\_/ __ \\");
		System.out
				.println(" |  | \\// __ \\_/ /_/  >  ___/|     \\   |  | \\/\\  ___/\\  ___/ ");
		System.out
				.println(" |__|  (____  /\\___  / \\___  >___  /   |__|    \\___  >\\___  >");
		System.out
				.println("            \\//_____/      \\/    \\/                \\/     \\/ ");
		System.out
				.println("\n An easy-to-use AlterIWnet server/client setup program written in Java");
		System.out.println(" v0.1.2 Release");
		System.out.println(" By Lawrence Adams 2013");


		while (true) {
			setupMenu();
		}
	}

	private static void setupMenu() {
		System.out
				.print("\nPlease enter option and press enter:\n	[1] Client mode\n	[2] Server mode\n	[3] About\n 	[4] Exit\n");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String sOption = "";

		try {
			sOption = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (sOption.equals("1")) {
			FileManage.generateConfig();
			FileManage.configSetup(Client.connectToServer());
			Server.startCOD();
			System.out.println("Starting COD, have fun!");
			System.out.println("Quitting this program");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.exit(0);
		} else if (sOption.equals("2")) {
			FileManage.generateConfig();
			FileManage.configSetup(Server.findIP());
			Server.startAlterIW();

			System.out.println("Spinning up new server thread...");
			discoveryThread.start();

			System.out.println("Please keep this file running!");
			Server.startCOD();
			while (true);
		} else if (sOption.equals("3")) {
			FileManage.about();
		} else if (sOption.equals("4")) {
			System.out.println("Exiting... Bye");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.exit(0);
		} else {
			System.out.println("Sorry, that wasn't recognised...");
		}
	}
}
