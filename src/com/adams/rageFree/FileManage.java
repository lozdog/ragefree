package com.adams.rageFree;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class FileManage {

	public static void configSetup(String servIP) {
		// TODO Fix...
		File f = new File("./alterIWnet.ini");

		FileInputStream fs = null;
		InputStreamReader in = null;
		BufferedReader br = null;

		StringBuffer sb = new StringBuffer();

		String textinLine;

		try {
			fs = new FileInputStream(f);
			in = new InputStreamReader(fs);
			br = new BufferedReader(in);

			while (true) {
				textinLine = br.readLine();
				if (textinLine == null)
					break;
				sb.append(textinLine);
			}

			int cnt1 = sb.indexOf("Server=");

			// sb.replace(cnt1, cnt1, System.lineSeparator());

			cnt1 = sb.indexOf("Server="); // Overloading original value of cnt1
			int cnt2 = sb.indexOf("WebHost=");
			sb.replace(cnt1, cnt2, "Server=" + servIP + System.lineSeparator());

			cnt2 = sb.indexOf("WebHost="); // Overloading original value of cnt2
			sb.replace(cnt1, cnt1, System.lineSeparator());

			int cnt3 = sb.indexOf("Nickname=");
			sb.replace(cnt3, cnt3, System.lineSeparator());

			fs.close();
			in.close();
			br.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			FileWriter fstream = new FileWriter(f);
			BufferedWriter outobj = new BufferedWriter(fstream);
			outobj.write(sb.toString());
			outobj.close();

		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
	}

	public static void generateConfig() {

		File f = new File("./alterIWnet.ini");
		if (f.exists() == false) {

			System.out.println("<<@WARNING >>No config file found... generating one now!");
			BufferedWriter writer = null;
			try {
				writer = new BufferedWriter(new FileWriter("alterIWnet.ini"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				writer.write("[Configuration]");
				writer.newLine();
				writer.write("Server=0.0.0.0");
				writer.newLine();
				writer.write("WebHost=auto");
				writer.newLine();
				writer.write("Nickname=default");
				writer.close();
			} catch (IOException er) {
				er.printStackTrace();
			} finally {
				System.out.println("<<@WARNING>>Done!\n");
			}
		}
	}

	public static void about() {
		// System.out.println("******************DEDICATION******************");
		// System.out.println("This program is dedicated to EML's ass.\n");
		System.out.println("******************DISCLAMER******************");
		System.out.println("By running this program I take NO responisibility");
		System.out
				.println("if your computer randomly catches fire, you get a month of");
		System.out
				.println("fatigues as you've done no prep, or you are reset to level 1.");
		System.out
				.println("That said, none of the above should happen... BUT if they do");
		System.out.println("that's not my fault.\n");
		System.out.println("******************ABOUT******************");
		System.out
				.println("This is an easy to use AlterIWNet client/server configuration");
		System.out
				.println("program written by Lawrence Adams. To use simply press");
		System.out
				.println("either 1 if you want to connect to someone's server");
		System.out
				.println("or 2 if you want to act as a host, and then enter. This'll do the rest ");
		System.out
				.println("for you - even Tom West (#bigrug) could manage this...");
		System.out.println("\nPress enter to continue");
		Scanner sc = new Scanner(System.in);
		while (!sc.nextLine().equals(""))
			;
	}
}
