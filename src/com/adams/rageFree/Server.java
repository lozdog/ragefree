package com.adams.rageFree;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Server {

	public static String findIP() {

		String localIP = "";

		try {
			InetAddress addr = InetAddress.getLocalHost();
			localIP = addr.toString();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			System.out.println("Sorry, looks like your not connected to a network, please try again later");
		}

		String[] afterHostName = localIP.split("/");

		String serverIP = "";
		serverIP = afterHostName[1];
		return serverIP;
	}

	@SuppressWarnings("unused")
	public static void startAlterIW() {
		Server.terminateAIWnet();
		long t0,t1;
        t0=System.currentTimeMillis();
        do{
            t1=System.currentTimeMillis();
        }
        while (t1-t0<1000);
		Runtime rt = Runtime.getRuntime();
		try {
			Process p1 = rt.exec("cmd /c start IWNetServer.exe");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Started IWNetServer");
		}
		System.out.println("Done!");
	}
	
	private static void terminateAIWnet(){
		
		Runtime rt = Runtime.getRuntime();
		System.out.println("Killing previous IWNetServer instances...");
		try {
			@SuppressWarnings("unused")
			Process p0 = rt.exec("taskkill /F /IM IWNetServer.exe");
			System.out.println("Any instances killed - awesome.");
		} catch (IOException e1) {
			e1.printStackTrace();
			System.out.println("No instance of IWNetServer.exe running - cool.");
		}
	}

	public static void startCOD() {
		 Runtime rt = Runtime.getRuntime();
		 try {
		 @SuppressWarnings("unused")
		 Process p = rt.exec("iw4mp.exe");
		 } catch (IOException e) {
			 System.out.println("\n*************ERROR**************");
			 System.out.println("<<Warning>> iw4mp.exe not found");
			 System.out.println("<<Warning>> Please ensure that rageFree is\n in your COD folder.");
			 System.out.println("<<Warning>> Will act as a host *without* playing");
		 }
	}

	public static String currentDir() {
		String fileDir = System.getProperty("user.dir");
		System.out.println(fileDir);
		return fileDir;

	}

}
