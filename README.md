# Welcome

Welcome to the rageFree.

## rageFree Features

rageFree is an easy-to-use AlterIWNet config setup program for LAN players.

It is written in JavaTM and is currently compiled for JRE 1.6 using JDK 1.7 with compiler complaince to 1.6

## Warning

I CANNOT take any responsibility for anything that results from the use of rageFree

This includes, but is not limited to:

	-> Your computer randomly catching fire
	-> Your playerstats being reset to level 1
	-> You getting 5 weeks fatigues for not doing any prep

That said, none of the above should happen.

## How to use

Make sure that rageFree is in your COD folder, and that the latest version of Java is installed

1. Run the progam, click yes if asked.
2. If you are connecting to someone else press "1" and then enter (You need to agree who is before hand)
3. If you are acting as a host, press "2" and then enter
4. COD should start

Have fun!